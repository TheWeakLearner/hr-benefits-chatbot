import pandas as pd
import openai
from scipy import spatial
import ast
import tiktoken
from flask import Flask, request, jsonify
from flask_cors import CORS

app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": ["http://localhost:8080", "http://127.0.0.1:8080"]}})


EMBEDDING_MODEL = "text-embedding-ada-002"
GPT_MODEL = "gpt-3.5-turbo"

openai.api_key = '<openai key>'
openai.organization = '<openai org id>'

#document_embeddings = pd.read_pickle("../data/benefit_doc_embeddings.pickle")
#document_embeddings.to_csv("../data/benefit_doc_embeddings.csv")
document_embeddings = pd.read_csv("../data/benefit_doc_embeddings.csv")

document_embeddings['embeddings'] = document_embeddings['embeddings'].apply(ast.literal_eval)
                                        
def strings_ranked_by_relatedness(
    query: str,
    df: pd.DataFrame,
    relatedness_fn=lambda x, y: 1 - spatial.distance.cosine(x, y),
    top_n: int = 100
) -> tuple[list[str], list[float]]:
    """Returns a list of strings and relatednesses, sorted from most related to least."""
    query_embedding_response = openai.Embedding.create(
        model=EMBEDDING_MODEL,
        input=query,
    )
    query_embedding = query_embedding_response["data"][0]["embedding"]
    strings_and_relatednesses = [
        ((row["page_num"], row["text"]), relatedness_fn(query_embedding, row["embeddings"][0]))
        for i, row in df.iterrows()
    ]
    strings_and_relatednesses.sort(key=lambda x: x[1], reverse=True)
    return strings_and_relatednesses[:top_n]

def num_tokens(text: str, model: str = GPT_MODEL) -> int:
    """Return the number of tokens in a string."""
    encoding = tiktoken.encoding_for_model(model)
    return len(encoding.encode(text))

def query_message(
    query: str,
    df: pd.DataFrame,
    model: str,
    token_budget: int
) -> str:
    """Return a message for GPT, with relevant source texts pulled from a dataframe."""
    introduction = ('Below is a list of sections from the employee benefits'
                    ' manual that are related to the query. Each section '
                    ' will be structed with a "relatedness score:", a '
                    ' "page number:" and the "Employee Benefits Text:".'
                    ' Use these sections to answer the subsequent question.'
                    ' If the answer cannot be found in the document'
                    ' then write "I am unable to find a sufficent answer to your question."')
    question = f"\n\nQuestion: {query}"
    excerpts = strings_ranked_by_relatedness(query, df, top_n=10)

    message = introduction
    for excerpt in excerpts:
        relatedness = excerpt[1]
        string = excerpt[0]
        next_section = (f'\n\nrelatedness score: {relatedness}\npage number:{string[0]}'
                        f'\nEmployee Benefits Text:"""\n{string[1]}\n"""')
        if (
            num_tokens(message + next_section + question, model=model)
            > token_budget
        ):
            break
        else:
            message += next_section
    return message + question


def ask(
    query: str,
    df: pd.DataFrame = document_embeddings,
    model: str = GPT_MODEL,
    token_budget: int = 4096 - 500,
) -> str:
    """Answers a query using GPT and a dataframe of relevant texts and embeddings."""
    message = query_message(query, df, model=model, token_budget=token_budget)

    messages = [
        {"role": "system", "content": "You answer questions about our employee benefits."},
        {"role": "user", "content": message},
    ]
    response = openai.ChatCompletion.create(
        model=model,
        messages=messages,
        temperature=0
    )
    response_message = response["choices"][0]["message"]["content"]
    return response_message

# ask("What are my health plan options?")

@app.route("/api/ask", methods=["POST"])
def api_ask():
    data = request.get_json()
    query = data.get("query")
    token_budget = data.get("token_budget", 4096 - 500)
    if not query:
        return jsonify({"error": "Missing query parameter"}), 400
    response_message = ask(query, token_budget=token_budget)
    print(response_message)
    return jsonify({"botresponse": response_message})

if __name__ == "__main__":
    app.run(debug=True)
