from flask import Flask, request, jsonify
from flask_cors import CORS
import openai

import PyPDF2
from nltk.tokenize import sent_tokenize
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
import tiktoken
import nltk
import pandas as pd

nltk.download('popular')

# models
EMBEDDING_MODEL = "text-embedding-ada-002"
GPT_MODEL = "gpt-3.5-turbo"

# Ensure you have set up your OpenAI API key
openai.api_key = '<openai key>'
openai.organization = '<openai org id>'

app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": ["http://localhost", "http://127.0.0.1:80"]}})

# Load your PDF
pdf_file = open('../pdfs/employee-benefits-reference-guide.pdf', 'rb')
read_pdf = PyPDF2.PdfReader(pdf_file)
number_of_pages = len(read_pdf.pages)

# Extract text from all pages
page_content = []
for page_number in range(number_of_pages):
    page = read_pdf.pages[page_number]
    text = page.extract_text()
    page_sentences = sent_tokenize(text)
    page_content.append(page_sentences)

pdf_embeddings = pd.DataFrame()
page_number = 0
for page in page_content:
    page_embeddings = []
    page_number = page_number + 1
    for sentence in page:
        response = openai.Embedding.create(model=EMBEDDING_MODEL, input=sentence)
        batch_embeddings = [e["embedding"] for e in response["data"]]
        page_embeddings.append(batch_embeddings)
    embeddings_df = pd.DataFrame({
        'page_num': page_number,
        'text': page,
        'embeddings': page_embeddings
    })
    pdf_embeddings = pd.concat([pdf_embeddings, embeddings_df])

pdf_embeddings.to_pickle("../data/benefit_doc_embeddings.pickle")